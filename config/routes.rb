Rails.application.routes.draw do
  root 'pages#home'
  resources :recipes do
    collection do
      get :search
    end
  end
end
