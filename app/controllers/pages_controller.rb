class PagesController < ApplicationController
  def home
    @recipes = Recipe.three_most_liked
  end
end
