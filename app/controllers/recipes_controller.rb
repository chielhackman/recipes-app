class RecipesController < ApplicationController
  def search
    @recipes = Recipe.search(params[:search])
  end

  def show
    @recipe = Recipe.find(params[:id])
  end
end
