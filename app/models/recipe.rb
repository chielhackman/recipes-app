class Recipe < ApplicationRecord
  scope :three_most_liked, -> { order(likes: :desc).first(3) }
  scope :search, -> (search) { where 'name LIKE ?', "%#{search}%" }
end
