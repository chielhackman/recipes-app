require 'rails_helper'

RSpec.feature 'Recipe preview links to recipe page' do
  scenario 'Visitor visits the home page and clicks on one of the most liked recipes' do
    create(:recipe, name: 'Spicy chicken', likes: 5)
    create(:recipe, name: 'Summer salad', likes: 9)
    create(:recipe, name: 'Sunday breakfast', likes: 7)
    create(:recipe, name: 'Roasted bread', likes: 4)

    visit '/'

    click_on 'Spicy chicken'

    expect(page).to have_text('The hottest')
  end

  scenario 'Visitor searches a recipe and clicks on one of the results' do
    create(:recipe, name: 'Spicy chicken', likes: 5)
    create(:recipe, name: 'Summer salad', likes: 9)
    create(:recipe, name: 'Sunday breakfast', likes: 7)
    create(:recipe, name: 'Roasted bread', likes: 4)

    visit '/'

    within '#search_recipe' do
      fill_in 'search', with: 'Spicy chicken'
      click_button 'search'
    end

    click_on 'Spicy chicken'

    expect(page).to have_text('The hottest')
  end
end
