require 'rails_helper'

RSpec.feature 'Searching recipes' do
  scenario 'Visitor searches for a recipe' do
    create(:recipe, name: 'Spicy chicken', likes: 5)
    create(:recipe, name: 'Summer salad', likes: 9)
    create(:recipe, name: 'Sunday breakfast', likes: 7)
    create(:recipe, name: 'Another recipe', likes: 4)

    visit '/'

    within '#search_recipe' do
      fill_in 'search', with: 'Spicy chicken'
      click_button 'search'
    end

    expect(page).to have_text('Spicy chicken')
    expect(page).to_not have_text('Summer salad')
  end
end
