require 'rails_helper'

RSpec.feature 'Displaying 3 most liked recipes' do
  scenario 'Visitor visits the home page' do
    create(:recipe, name: 'Spicy chicken', likes: 5)
    create(:recipe, name: 'Summer salad', likes: 9)
    create(:recipe, name: 'Sunday breakfast', likes: 7)
    create(:recipe, name: 'Roasted bread', likes: 4)

    visit '/'

    expect(page).to have_text('Spicy chicken')
    expect(page).to have_text('Summer salad')
    expect(page).to have_text('Sunday breakfast')
    expect(page).to_not have_text('Roasted bread')
  end
end
