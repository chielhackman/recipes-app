FactoryBot.define do
  factory :recipe do
    name { 'Spicy chicken' }
    description { 'The hottest spicy chicken you ever had!' }
    likes { 0 }
  end
end
